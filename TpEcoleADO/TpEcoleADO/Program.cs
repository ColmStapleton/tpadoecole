﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TpEcoleADO.DAL;

namespace TpEcoleADO
{
    class Program
    {
        static void Main(string[] args)
        {
            AffichageMenu();
            string saisie = Console.ReadLine();
            StudentDAL sDal = new StudentDAL();

            while (saisie != "4")
            {
                switch (saisie)
                {
                    case ("1"):
                        sDal.FindAll();
                        break;
                    case ("2"):
                        sDal.InsertToDb();
                        break;
                    case ("3"):
                        sDal.UpdateDbById();
                        break;
                    default:
                        Console.WriteLine("Invalid answer. Try again");
                        break;
                }
                AffichageMenu();
                saisie = Console.ReadLine();
            }
        }

        static private void AffichageMenu()
        {
            Console.WriteLine("Menu:\n1) Display the list of students");
            Console.WriteLine("2) Add a new student");
            Console.WriteLine("3) Update a student's file");
            Console.WriteLine("4) Exit the program");
        }
    }
}
