﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TpEcoleADO.Models;

namespace TpEcoleADO.DAL
{
    class StudentDAL
    {
        //1) Méthode pour récupérer tous les élèves de la base
        public void FindAll()
        {
            List<Student> lStudent = new List<Student>();

            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText = String.Format("SELECT * FROM Eleves");

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DateTime fullDate = (DateTime)reader["BirthDate"];

                            Student student = new Student()
                            {
                                Id = (int)reader["Id"],
                                Nom = reader["LastName"].ToString(),
                                Prenom = reader["FirstName"].ToString(),
                                DateNaissance = new DateTime(fullDate.Year, fullDate.Month, fullDate.Day),
                                Poids = (int)reader["Weight"],
                                Annee = (int)reader["Grade"]
                            };
                            lStudent.Add(student);
                        }
                    }
                }
            }

            foreach (Student student in lStudent)
            {
                Console.WriteLine(student.ToString());
                Console.WriteLine("***************************************************************");
            }
            Console.WriteLine("Total number of students : {0}", lStudent.Count);
        }

        //2) Méthode pour ajouter un élève dans la base
        public void InsertToDb()
        {
            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    Console.WriteLine("Please provide the student's information:");
                    string firstName = getName("First name");
                    string lastName = getName("Last name");
                    DateTime birthDate = getBirthDate();
                    int weight = getWeight();
                    int grade = getGrade();

                    command.CommandText = String.Format("INSERT INTO Eleves (LastName, FirstName, BirthDate, Weight, Grade) VALUES (@Nom,@Prenom,@Date,@Poids,@Annee)");
                    command.Parameters.AddWithValue("@Nom", lastName);
                    command.Parameters.AddWithValue("@Prenom", firstName);
                    command.Parameters.AddWithValue("@Date", birthDate);
                    command.Parameters.AddWithValue("@Poids", weight);
                    command.Parameters.AddWithValue("@Annee", grade);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.InsertCommand = command;
                    int numLignes = dataAdapter.InsertCommand.ExecuteNonQuery();

                    if (numLignes > 0)
                    {
                        Console.WriteLine("=> The student '{0} {1}' has been successfully added to the database.", firstName, lastName);
                    }
                }
            }
        }

        //3) Méthode pour modifier un élève dans la base
        public void UpdateDbById()
        {
            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    Console.WriteLine("Which student's file would you like to edit (enter Id)? :");
                    string id = Console.ReadLine();
                    string firstName = getName("First name");
                    string lastName = getName("Last name");
                    DateTime birthDate = getBirthDate();
                    int weight = getWeight();
                    int grade = getGrade();

                    command.CommandText = String.Format("UPDATE Eleves SET LastName = @LastName, FirstName = @FirstName, BirthDate = @BirthDate, Weight = @Weight, Grade = @Grade WHERE Id = @Id");
                    command.Parameters.AddWithValue("@LastName", lastName);
                    command.Parameters.AddWithValue("@FirstName", firstName);
                    command.Parameters.AddWithValue("@BirthDate", birthDate);
                    command.Parameters.AddWithValue("@Weight", weight);
                    command.Parameters.AddWithValue("@Grade", grade);
                    command.Parameters.AddWithValue("@Id", id);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.UpdateCommand = command;
                    int numLignes = dataAdapter.UpdateCommand.ExecuteNonQuery();

                    if (numLignes > 0)
                    {
                        Console.WriteLine("=> The student file has been successfully updated for {0} {1}.",firstName, lastName);
                    }
                    else
                    {
                        Console.WriteLine("=> No student file has been modified for Id = {0}.",id);
                    }
                }
            }
        }

        //Méthode privée pour se connecter à la base
        private SqlConnection ConnectToDB()
        {
            SqlConnection connect = new SqlConnection();
            ConnectionStringSettings connex = ConfigurationManager.ConnectionStrings["ConnectionEcole"];

            connect.ConnectionString = connex.ConnectionString;
            connect.Open();
            return connect;
        }

        private DateTime getBirthDate ()
        {
            DateTime birthDate = new DateTime();

            Boolean validDate = false;
            while (!validDate)
            {
                Console.Write("> BirthDate: ");
                validDate = DateTime.TryParse(Console.ReadLine(), out birthDate);
                if (validDate)
                {
                    if (birthDate.Year > DateTime.Now.Year || birthDate.Year < 1900)
                    {
                        validDate = false;
                    }
                }
            }

            return birthDate;
        }

        private int getWeight()
        {
            int weight = 0;
            Boolean validWeight = false;

            while (!validWeight)
            {
                Console.Write("> Weight: ");
                validWeight = Int32.TryParse(Console.ReadLine(), out weight);
                if (validWeight)
                {
                    if (weight < 20 || weight > 300)
                    {
                        validWeight = false;
                    }
                }
            }
            return weight;
        }

        private int getGrade()
        {
            int grade = 0;
            Boolean validGrade = false;

            while (!validGrade)
            {
                Console.Write("> Grade: ");
                validGrade = Int32.TryParse(Console.ReadLine(), out grade);
                if (validGrade)
                {
                    if (grade < 1 || grade > 12)
                    {
                        validGrade = false;
                    }
                }
            }

            return grade;
        }

        private string getName(string affichage)
        {
            string name = "";

            Regex reg = new Regex(@"[\d\t\#\~\'\,\!\?\:\;\,\.\$\£\%\*\^\@\&]");

            Boolean notValidName = true;
            while (notValidName)
            {
                Console.Write("> {0}: ",affichage);
                name = Console.ReadLine();
                notValidName = reg.IsMatch(name);
                if (name.Length < 1 || name.Length > 50)
                {
                    notValidName = true;
                }
            }
            return name;
        }
    }
}
