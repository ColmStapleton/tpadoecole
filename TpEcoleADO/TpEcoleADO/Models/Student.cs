﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TpEcoleADO.Models
{
    public class Student
    {

        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _prenom;

        public string Prenom
        {
            get { return _prenom; }
            set { _prenom = value; }
        }

        private string _nom;

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        private DateTime _dateNaissance;

        public DateTime DateNaissance
        {
            get { return _dateNaissance; }
            set { _dateNaissance = value; }
        }

        private int _poids;

        public int Poids
        {
            get { return _poids; }
            set { _poids = value; }
        }

        private int _annee;

        public int Annee
        {
            get { return _annee; }
            set { _annee = value; }
        }

        public Student()
        {

        }

        public Student(string p, string n, DateTime d, int poids, int a)
        {
            this.Prenom = p;
            this.Nom = n;
            this.DateNaissance = d;
            this.Poids = poids;
            this.Annee = a;
        }

        public override string ToString()
        {
            return String.Format("Student: {0} {1} (Id {2})\nWeight: {3}, date of birth: {4}\nGrade: {5}", this.Prenom, this.Nom, this.Id, this.Poids, this.DateNaissance, this.Annee);
        }
    }
}
